#!/bin/bash

ncat -v -l -p $1 -c '
while read -r cmd; do
  case $cmd in
    q) break ;;
    *) $cmd
  esac
done
'
