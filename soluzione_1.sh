#!/bin/bash
NOME_FILE=$1

NUMERI=`cat $NOME_FILE | awk -F ' ' '(NR>1)'{'print $1'}`
SOMMA=`echo $NUMERI | tr ' ' '\n' | awk '{somma+=$1};END {print somma}'`
LETTERE=(`cat $NOME_FILE | awk -F ' ' '(NR>1)'{'print $2'}`)
SIMBOLI=(`cat $NOME_FILE | awk -F ' ' '(NR>1)'{'print $3'}`)
echo "I numeri nel file sono: " $NUMERI
echo "La loro somma è: " $SOMMA
echo "Le lettere sono: " ${LETTERE[@]}
echo "I simboli sono: " ${SIMBOLI[@]}
COUNT=0
while [ $COUNT -lt ${#LETTERE[@]} ]; do
    echo "${LETTERE[$COUNT]} = ${SIMBOLI[$COUNT]}"
    let COUNT=COUNT+1
done

