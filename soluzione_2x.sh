#!/bin/bash

CPU_COL="3"
MEM_COL="4"
DEF_COL=$CPU_COL
prev="c"
SORT_BY_CPU="-%cpu"
SORT_BY_MEM="-%mem"
SORT_BY=$SORT_BY_CPU

while [ true ]
do
    clear
    read -rsn1 -t0.010 choose
    [[ -z "$choose" ]]     && choose=$prev || prev=$choose 
    [[ "$choose" == "c" ]] && { DEF_COL=$CPU_COL; SORT_BY=$SORT_BY_CPU; }
    [[ "$choose" == "m" ]] && { DEF_COL=$MEM_COL; SORT_BY=$SORT_BY_MEM; }

    PROCESSES=`ps aux --sort=$SORT_BY | head -n 5`

    echo "${PROCESSES}" | awk -v col="$DEF_COL" '{printf("%s\t%s\t%s\t", $1,$2,$col)} {for(i=11; i<=NF; i++) printf "%s ", $i; printf "\n" }'
    echo "Press 'c' or 'm' to switch to CPU or MEM consumption"

    sleep 2

done

