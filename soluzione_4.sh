#!/bin/bash
#eseguire con watch -n 3600 ./soluzione_4.sh

LOG_PROC_FILE="processes.log"
DEF_COL=3

CURTIME=`date "+%Y%m%d-%H%M%S"`
LOG_FILE="$CURTIME.log"
echo -e "USER\tPID\tCPU%\tPROCESS NAME" >> $LOG_FILE

x=1
while [ $x -le 1800 ]
do
    PROCESSES=`ps aux | sort -nrk "$DEF_COL,$DEF_COL" | head -n 1`
    USER=`echo ${PROCESSES} | awk '{printf("%s", $1)}'`
    PID=`echo ${PROCESSES} | awk '{printf("%s", $2, $3)}'`
    CPU=`echo ${PROCESSES} | awk '{printf("%s", $3)}'`
    PROC=`echo ${PROCESSES} | awk '{for(i=11; i<=NF; i++) printf("%s ", $i);}'`

    if ! grep -qs "$PID" "$LOG_FILE"; then
        echo -e "$USER\t$PID\t$CPU\t$PROC" >> $LOG_FILE
    fi

    sleep 2
    x=$(($x + 1))
done
tar cf $CURTIME.tar.gz $LOG_FILE
